<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1438108309.
 * Generated on 2015-07-28 18:31:49 by vagrant
 */
class PropelMigration_1438108309
{
    public $comment = '';

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'pgsql' => '
CREATE TABLE "users"
(
    "id" serial NOT NULL,
    "first_name" VARCHAR(255) NOT NULL,
    "last_name" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

CREATE TABLE "permissions"
(
    "id" serial NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

CREATE TABLE "users_permissions"
(
    "user_id" INTEGER NOT NULL,
    "permission_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("user_id","permission_id")
);

ALTER TABLE "users_permissions" ADD CONSTRAINT "users_permissions_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

ALTER TABLE "users_permissions" ADD CONSTRAINT "users_permissions_fk_0429e3"
    FOREIGN KEY ("permission_id")
    REFERENCES "permissions" ("id");
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'pgsql' => '
DROP TABLE IF EXISTS "users" CASCADE;

DROP TABLE IF EXISTS "permissions" CASCADE;

DROP TABLE IF EXISTS "users_permissions" CASCADE;
',
);
    }

}