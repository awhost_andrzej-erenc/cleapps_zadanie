@extends('layouts.layout')

@section('content')
<a href="{{route('users.index')}}">Back to list</a>
<h2>Create new user</h2>
<hr/>
@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
</ul>
@endif

{!! Form::Open(['url' => route('users.store')]) !!}
<div class='form-group'>
    {!! Form::label('first_name', 'First Name*:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('email', 'Email*:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('password', 'Password*:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::submit('Add user', ['class' => 'btn btn-primary form-control']) !!}
</div>
{!! Form::Close() !!}
@stop