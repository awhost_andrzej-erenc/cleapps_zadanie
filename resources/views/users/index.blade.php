@extends('layouts.layout')

@section('content')

<h2>Users List:</h2>
<a href="{{route('users.create')}}">Create new</a>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Imię</th>
            <th>Nazwisko</th>
            <th>Email</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse($users as $k)
        <tr>
            <td>{{$k->getId()}}</td>
            <td>{{$k->getFirstName()}}</td>
            <td>{{$k->getLastName()}}</td>
            <td>{{$k->getEmail()}}</td>
            <td><div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Actions
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="{{route('users.show', $k->getId())}}">Show</a></li>
                        <li><a href="{{route('users.edit', $k->getId())}}">Edit</a></li>
                        <li><a href="{{route('users.destroy', $k->getId())}}">Delete</a></li>
                    </ul>
                </div></td>
        </tr>
        @empty
    <td colspan="3">No users found.</td>
    @endforelse
</tbody>
</table>
@stop