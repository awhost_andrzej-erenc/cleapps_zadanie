<?php
/*
 * Copyright (C) 2015 Andrzej Erenc <andrzej.erenc@awhost.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Repositories;

use App\Models\UserQuery;

/**
 * Description of UserRepository
 *
 * @author Andrzej Erenc <andrzej.erenc@awhost.pl>
 */
class UserRepository implements \App\Controllers\UserRepository
{

    public function getAll()
    {
        return UserQuery::create()->orderById('desc');
    }

    public function getOneById($id)
    {
        return UserQuery::create()->findPK($id);
    }

    public function deleteById($id)
    {
        $user = UserQuery::create()->findPK($id);
        $user->delete();
    }

    public function save($id, array $user)
    {
        UserQuery::create()
            ->findPK($id)
            ->update($user);
    }

    public function update($id, array $data)
    {
        
    }

    public function create(array $data)
    {
        $user = new \App\Models\User();
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);
        $user->setEmail($data['email']);
        $user->setPassword($data['password']);
        $user->save();
    }
}
