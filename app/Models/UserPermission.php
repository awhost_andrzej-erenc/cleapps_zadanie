<?php

namespace App\Models;

use App\Models\Base\UserPermission as BaseUserPermission;

/**
 * Skeleton subclass for representing a row from the 'user_permission' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserPermission extends BaseUserPermission
{

}
