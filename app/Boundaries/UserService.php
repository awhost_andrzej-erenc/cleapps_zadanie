<?php
/*
 * Copyright (C) 2015 Andrzej Erenc <andrzej.erenc@awhost.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Boundaries;

use \App\Repositories\UserRepository;

/**
 * Description of UserService
 *
 * @author Andrzej Erenc <andrzej.erenc@awhost.pl>
 */
class UserService
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAll(\App\Requests\GetAllUsersRequest $request)
    {
        $users = $this->userRepository->getAll();

        return new \App\Responses\GetAllUsersResponse($users);
    }

    public function getOne(\App\Requests\GetOneUsersRequest $request)
    {
        $user = $this->userRepository->getOneById($request->id);

        return new \App\Responses\GetOneUserResponse($user);
    }

    public function deleteOne(\App\Requests\DeleteOneUsersRequest $request)
    {
        return $this->userRepository->deleteById($request->id);
    }

    public function update(\App\Requests\UpdateOneUsersRequest $request)
    {
        return $this->userRepository->update($request->id, $request->data);
    }

    public function create(\App\Requests\CreateUserRequest $request)
    {
        return $this->userRepository->create($request->data);
    }
}
