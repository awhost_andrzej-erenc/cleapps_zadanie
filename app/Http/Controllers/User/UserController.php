<?php
/*
 * Copyright (C) 2015 Andrzej Erenc <andrzej.erenc@awhost.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use \App\Boundaries\UserService;
use \App\Http\Requests\CreateUserRequest;

/**
 * Description of PermissionController
 *
 * @author Andrzej Erenc <andrzej.erenc@awhost.pl>
 */
class UserController extends Controller
{

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        $users = $this->userService->getAll(new \App\Requests\GetAllUsersRequest());
        return view('users.index')->with('users', $users->users);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(CreateUserRequest $request)
    {
        $this->userService->create(new \App\Requests\CreateUserRequest($request->all()));

        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = $this->userService->getOne(new \App\Requests\GetOneUserRequest($id));

        return redirect()->route('users.edit')->with('user', $user->user);
    }

    public function update($id, \App\Http\Requests\UpdateUserRequest $request)
    {
        $this->userService->update(new \App\Requests\UpdateUserRequest($id, $request->all()));
    }
}
